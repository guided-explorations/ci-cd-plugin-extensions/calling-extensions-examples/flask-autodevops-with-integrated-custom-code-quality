---
dashboard: Environment metrics
priority: 1
panel_groups:
- group: System metrics (Kubernetes)
  priority: 15
  panels:
  - title: Memory Usage (Total)
    type: area-chart
    y_label: Total Memory Used (GB)
    weight: 4
    metrics:
    - id: system_metrics_kubernetes_container_memory_total
      query_range: avg(sum(container_memory_usage_bytes{container_name!="POD",pod_name=~"^%{ci_environment_slug}-(.*)",namespace="%{kube_namespace}"})
        by (job)) without (job)  /1024/1024/1024
      label: Total (GB)
      unit: GB
      metric_id: 374
  - title: Core Usage (Total)
    type: area-chart
    y_label: Total Cores
    weight: 3
    metrics:
    - id: system_metrics_kubernetes_container_cores_total
      query_range: avg(sum(rate(container_cpu_usage_seconds_total{container_name!="POD",pod_name=~"^%{ci_environment_slug}-(.*)",namespace="%{kube_namespace}"}[15m]))
        by (job)) without (job)
      label: Total (cores)
      unit: cores
      metric_id: 375
  - title: Memory Usage (Pod average)
    type: line-chart
    y_label: Memory Used per Pod (MB)
    weight: 2
    metrics:
    - id: system_metrics_kubernetes_container_memory_average
      query_range: avg(sum(container_memory_usage_bytes{container_name!="POD",pod_name=~"^%{ci_environment_slug}-([^c].*|c([^a]|a([^n]|n([^a]|a([^r]|r[^y])))).*|)-(.*)",namespace="%{kube_namespace}"})
        by (job)) without (job) / count(avg(container_memory_usage_bytes{container_name!="POD",pod_name=~"^%{ci_environment_slug}-([^c].*|c([^a]|a([^n]|n([^a]|a([^r]|r[^y])))).*|)-(.*)",namespace="%{kube_namespace}"})
        without (job)) /1024/1024
      label: Pod average (MB)
      unit: MB
      metric_id: 376
  - title: 'Canary: Memory Usage (Pod Average)'
    type: line-chart
    y_label: Memory Used per Pod (MB)
    weight: 2
    metrics:
    - id: system_metrics_kubernetes_container_memory_average_canary
      query_range: avg(sum(container_memory_usage_bytes{container_name!="POD",pod_name=~"^%{ci_environment_slug}-canary-(.*)",namespace="%{kube_namespace}"})
        by (job)) without (job) / count(avg(container_memory_usage_bytes{container_name!="POD",pod_name=~"^%{ci_environment_slug}-canary-(.*)",namespace="%{kube_namespace}"})
        without (job)) /1024/1024
      label: Pod average (MB)
      unit: MB
      track: canary
      metric_id: 377
  - title: Core Usage (Pod Average)
    type: line-chart
    y_label: Cores per Pod
    weight: 1
    metrics:
    - id: system_metrics_kubernetes_container_core_usage
      query_range: avg(sum(rate(container_cpu_usage_seconds_total{container_name!="POD",pod_name=~"^%{ci_environment_slug}-([^c].*|c([^a]|a([^n]|n([^a]|a([^r]|r[^y])))).*|)-(.*)",namespace="%{kube_namespace}"}[15m]))
        by (job)) without (job) / count(sum(rate(container_cpu_usage_seconds_total{container_name!="POD",pod_name=~"^%{ci_environment_slug}-([^c].*|c([^a]|a([^n]|n([^a]|a([^r]|r[^y])))).*|)-(.*)",namespace="%{kube_namespace}"}[15m]))
        by (pod_name))
      label: Pod average (cores)
      unit: cores
      metric_id: 378
  - title: 'Canary: Core Usage (Pod Average)'
    type: line-chart
    y_label: Cores per Pod
    weight: 1
    metrics:
    - id: system_metrics_kubernetes_container_core_usage_canary
      query_range: avg(sum(rate(container_cpu_usage_seconds_total{container_name!="POD",pod_name=~"^%{ci_environment_slug}-canary-(.*)",namespace="%{kube_namespace}"}[15m]))
        by (job)) without (job) / count(sum(rate(container_cpu_usage_seconds_total{container_name!="POD",pod_name=~"^%{ci_environment_slug}-canary-(.*)",namespace="%{kube_namespace}"}[15m]))
        by (pod_name))
      label: Pod average (cores)
      unit: cores
      track: canary
      metric_id: 379
  - title: Knative function invocations
    type: area-chart
    y_label: Invocations
    weight: 1
    metrics:
    - id: system_metrics_knative_function_invocation_count
      query_range: sum(ceil(rate(istio_requests_total{destination_service_namespace="%{kube_namespace}",
        destination_service=~"%{function_name}.*"}[1m])*60))
      label: invocations / minute
      unit: requests
      metric_id: 964
